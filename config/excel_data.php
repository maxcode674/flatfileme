<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Config for parse Flat file
    |--------------------------------------------------------------------------
    |
    | There are configs for three tabs parsing and write excel files
    |
    |Default sheets indexes
    |(
    |   [0] => icons
    |   [1] => International URLs
    |   [2] => International Settings
    |   [3] => DropdownSizer
    |   [4] => Instructions
    |   [5] => Images
    |   [6] => International Translations
    |   [7] => Data Validation
    |   [8] => International Data
    |   [9] => Example
    |   [10] => Data Definitions
    |   [11] => Template
    |   [12] => Browse Data
    |   [13] => Valid Values
    |   [14] => Dropdown Lists
    |   [15] => AttributePTDMAP
    |)
    |
    */

    'template' => [
        'sheet_name' => ["en" => "Template", "de" => "Vorlage"],
        'sheet_index' => 11, // Note that sheets are indexed from 0
        'headers_row' => 3,
    ],

    'data_definitions' => [
        'sheet_name' => ["en" => "Data Definitions", "de" => "Datendefinitionen"],
        'sheet_index' => 10, // Note that sheets are indexed from 0
        'color_column' => 1, // 'A'
        'field_name_column' => 2, // 'B'
        'label_name_column' => 3, // 'C'
        'accepted_values_column' => 4, // 'D'
        'example_column' => 5, // 'E'
        'required_check_column' => 6, // 'F',
        'start_row' => 4
    ],

    'valid_values' => [
        'sheet_name' => ["en" => "Valid Values", "de" => "Gültige Werte"],
        'sheet_index' => 13, // Note that sheets are indexed from 0
        'headers_row' => 2,
    ],

    /*
    |--------------------------------------------------------------------------
    | Config for parse Listing Report
    |--------------------------------------------------------------------------
    |
    | There are config for tab parsing and write excel files
    |
    */

    'listing_report' => [
        'headers_row' => 1,
        'sheet_index' => 0
    ],

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main payment data
    |--------------------------------------------------------------------------
    |
    |
    */

    'local' => [
        'monthly_plan' => [
            'id' => env('FLATFILEME_MONTHLY_PLAN'),
            'description' => 'This a monthly plan description',
            'name' => 'Monthly'
        ],
    ],

    'production' => [
        'monthly_plan' => [
            'id' => env('FLATFILEME_MONTHLY_PLAN'),
            'description' => "This a monthly plan description",
            'name' => "Monthly"
        ],
    ],

    'admin' => [
        'monthly_plan' => [
            'id' => env('FLATFILEME_MONTHLY_PLAN'),
            'description' => 'This a monthly plan description',
            'name' => 'Monthly'
        ],
    ]

];

<?php


namespace App\Services;

use App\Models\ExcelData;
use App\Models\ExcelListing;
use App\Models\UserFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use \PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;

class ExcelService
{
    protected $config;
    protected $worksheetList;

    public function __construct()
    {
        $this->config = config("excel_data");
    }

    /**
     * @param UserFile $userFile
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function prepareExcelData(UserFile $userFile)
    {
        //insure that file exists
        if (Storage::exists($userFile->link)) {
            //prepare file from storage and put it to storage/app/public folder
            $localPath = 'public/' . $userFile->link;
            Storage::disk('local')->put($localPath, Storage::get($userFile->link));

            $inputFileName = storage_path('app/' . $localPath);
            $inputFileType = 'Xlsx';

            /**  Create a new Reader of the type defined in $inputFileType  **/
            $reader = IOFactory::createReader($inputFileType);
            /** Get worksheet list **/
            $this->worksheetList = $reader->listWorksheetNames($inputFileName);

            if ($userFile->type == UserFile::FLAT_FILE) {
                $sheetnames = [
                    $this->worksheetList[Arr::get($this->config, 'data_definitions.sheet_index')],
                    $this->worksheetList[Arr::get($this->config, 'template.sheet_index')],
                    $this->worksheetList[Arr::get($this->config, 'valid_values.sheet_index')]
                ];
                /**  Load $inputFileName to a Spreadsheet Object  **/
                $reader->setLoadSheetsOnly($sheetnames);
            }

            $spreadsheet = $reader->load($inputFileName);

            /** Get 'Template' tab data **/
            $template = $userFile->type == UserFile::FLAT_FILE
                ? $this->readTemplateTab($spreadsheet)
                : $this->readTemplateTab($spreadsheet, "listing_report");
            /** Get 'Valid Values' tab data **/
            $validValues = $userFile->type == UserFile::FLAT_FILE
                ? $this->readValidValuesTab($spreadsheet)
                : [];
            /** Get 'Data Definitions' tab data */
            $dataDefinitions = $userFile->type == UserFile::FLAT_FILE
                ? $this->readDataDefinitionsTab($spreadsheet)
                : [];
            /** Prepare item model from 'Template' tab*/
            $itemModel = $userFile->type == UserFile::FLAT_FILE
                ? $this->getItemModelFromTab($spreadsheet)
                : $this->getItemModelFromTab($spreadsheet, "listing_report");;

            // inserting excel data to database
            ExcelData::create([
                'user_file_id' => $userFile->id,
                'template' => $template,
                'data_definitions' => $dataDefinitions,
                'valid_values' => $validValues,
                'item_model' => $itemModel
            ]);

            //remove temporary directory
            $destroyPreviousPath = 'public/user_files/' . auth()->id();
            if (Storage::disk('local')->exists($destroyPreviousPath)) {
                Storage::disk('local')->deleteDirectory($destroyPreviousPath); //remove directory
            }
        }

    }

    public function getItemModelFromTab(Spreadsheet $spreadsheet, $tab = "template")
    {
        $itemModel = [];
        // Retrieve the worksheet
        $templateSheet = $spreadsheet->getSheetByName($this->worksheetList[Arr::get($this->config, $tab.'.sheet_index')]);
        // Get the highest row and column numbers referenced in the worksheet
        $highestData = $this->getHighestDataInfoBySheet($templateSheet);

        //row with headers columns
        $headersRow = Arr::get($this->config, $tab.".headers_row");
        for ($col = 1; $col <= $highestData["column_index"]; $col++) {
            $itemModel[$templateSheet->getCellByColumnAndRow($col, $headersRow)->getValue()] = null;
        }

        return $itemModel;
    }

    public function getHighestDataInfoBySheet($sheet)
    {
        $highestDataColumn = $sheet->getHighestDataColumn(); // e.g 'F'
        return [
            "row" => $sheet->getHighestDataRow(), // e.g. 10
            "column" => $highestDataColumn,
            "column_index" => Coordinate::columnIndexFromString($highestDataColumn) // e.g. 5
        ];
    }

    public function readTemplateTab(Spreadsheet $spreadsheet, $tab = "template")
    {
        $result = [];
        // Retrieve the worksheet
        $templateSheet = $spreadsheet->getSheetByName($this->worksheetList[Arr::get($this->config, $tab.'.sheet_index')]);
        // Get the highest row and column numbers referenced in the worksheet
        $highestData = $this->getHighestDataInfoBySheet($templateSheet);

        //row with headers columns
        $headersRow = Arr::get($this->config, $tab.".headers_row");
        //iterate per row
        for ($row = $headersRow + 1; $row <= $highestData["row"]; $row++) {
            //iterate per column
            $rowData = [];
            //counted filled cells per row
            $filledCells = 0;
            for ($col = 1; $col <= $highestData["column_index"]; $col++) {
                $cellValue = $templateSheet->getCellByColumnAndRow($col, $row)->getValue();
                $headerValue = $templateSheet->getCellByColumnAndRow($col, $headersRow)->getValue();
                // set uppercase if parent or child
                if ($headerValue == "parent_child") {
                    $rowData[$headerValue] = ucfirst($cellValue);
                } else {
                    $rowData[$headerValue] = $cellValue;
                }

                if (!is_null($cellValue))
                    $filledCells++;
            }
            if ($filledCells > 0) {
                array_push($result, $rowData);
            }
        }

        return $result;
    }

    public function readValidValuesTab(Spreadsheet $spreadsheet)
    {
        $validData = [];
        // Retrieve the worksheet called 'Valid Values'
        $validValuesSheet = $spreadsheet->getSheetByName($this->worksheetList[Arr::get($this->config, 'valid_values.sheet_index')]);
        // Get the highest row and column numbers referenced in the worksheet
        $highestData = $this->getHighestDataInfoBySheet($validValuesSheet);
        //row with headers columns
        $headersRow = Arr::get($this->config, "valid_values.headers_row");
        for ($col = 1; $col <= $highestData["column_index"]; $col++) {
            $columnData = [];
            for ($row = $headersRow + 1; $row <= $highestData["row"]; $row++) {
                if (is_null($validValuesSheet->getCellByColumnAndRow($col, $row)->getValue())) {
                    break;
                } else {
                    $columnData[] = $validValuesSheet->getCellByColumnAndRow($col, $row)->getValue();
                }
            }
            //add validation values to main array
            $validData[$validValuesSheet->getCellByColumnAndRow($col, $headersRow)->getValue()] = $columnData;
        }

        return $validData;
    }

    public function readDataDefinitionsTab($spreadsheet)
    {
        $requiredData = [];
        // Retrieve the worksheet called 'Data Definitions';
        $dataDefinitionsSheet = $spreadsheet->getSheetByName($this->worksheetList[Arr::get($this->config, 'data_definitions.sheet_index')]);
        // Get the highest row and column numbers referenced in the worksheet
        $highestData = $this->getHighestDataInfoBySheet($dataDefinitionsSheet);
        //columns, that need checked
        $colorColumn = Arr::get($this->config, "data_definitions.color_column");
        $fieldNameColumn = Arr::get($this->config, "data_definitions.field_name_column");
        $labelNameColumn = Arr::get($this->config, "data_definitions.label_name_column");
        $acceptedValuesColumn = Arr::get($this->config, "data_definitions.accepted_values_column");
        $exampleColumn = Arr::get($this->config, "data_definitions.example_column");
        $requiredCheckColumn = Arr::get($this->config, "data_definitions.required_check_column");
        // start row
        $row = Arr::get($this->config, "data_definitions.start_row");
        for ($row; $row <= $highestData["row"]; $row++) {
            //check if this row not merged cells
            if (!$dataDefinitionsSheet->getCellByColumnAndRow($fieldNameColumn, $row)->getMergeRange()) {
                $cellColor = '#' . $dataDefinitionsSheet->getCellByColumnAndRow($colorColumn, $row)->getStyle()->getFill()->getStartColor()->getRGB();
                $cellFieldName = $dataDefinitionsSheet->getCellByColumnAndRow($fieldNameColumn, $row)->getValue();
                $cellLabelName = $dataDefinitionsSheet->getCellByColumnAndRow($labelNameColumn, $row)->getValue();
                $cellAcceptedValues = $dataDefinitionsSheet->getCellByColumnAndRow($acceptedValuesColumn, $row)->getValue();
                $cellExample = $dataDefinitionsSheet->getCellByColumnAndRow($exampleColumn, $row)->getValue();
                $isRequired = strtolower($dataDefinitionsSheet->getCellByColumnAndRow($requiredCheckColumn, $row)->getValue()) == "required";
                //now check if field name column contain range like "occasion_type1 - occasion_type27" then add for each occasion_type1, occasion_type2 ...
                if (strpos($cellFieldName, "1 - ") > 0) {
                    //get only field name without digits and last index example for occasion27 is 27
                    list($name, $last_ind) = explode("1 - ", $cellFieldName);
                    $last_ind = (int)filter_var($last_ind, FILTER_SANITIZE_NUMBER_INT);
                    for ($ind = 1; $ind <= $last_ind; $ind++) {
                        $requiredData[$name . $ind]['isRequired'] = $isRequired;
                        $requiredData[$name . $ind]['label_name'] = $cellLabelName;
                        $requiredData[$name . $ind]['accepted_values'] = $cellAcceptedValues;
                        $requiredData[$name . $ind]['example'] = $cellExample;
                        $requiredData[$name . $ind]['color'] = $cellColor;
                    }
                } else {
                    $requiredData[$cellFieldName]['isRequired'] = $isRequired;
                    $requiredData[$cellFieldName]['label_name'] = $cellLabelName;
                    $requiredData[$cellFieldName]['accepted_values'] = $cellAcceptedValues;
                    $requiredData[$cellFieldName]['example'] = $cellExample;
                    $requiredData[$cellFieldName]['color'] = $cellColor;
                }
            }
        }

        return $requiredData;
    }

    public function writeExcelData(ExcelData $excelData, $inputFileName, $filename, $type)
    {
        $inputFileType = 'Xlsx';

        /**  Create a new Reader of the type defined in $inputFileType  **/
        $reader = IOFactory::createReader($inputFileType);
        $this->worksheetList = $reader->listWorksheetNames($inputFileName);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($inputFileName);

        return $this->writeTemplateTab($spreadsheet, $filename, $excelData->template, $type);
    }

    public function writeTemplateTab(Spreadsheet $spreadsheet, $filename, $data = null, $type = 1)
    {
        $tab = $type == UserFile::FLAT_FILE ? 'template' : "listing_report";
        //need create directory by Storage facade because Phpspreadsheet can't
        $storagePath = 'public/user_files/' . auth()->id() . '/downloads/';
        Storage::disk('local')->makeDirectory($storagePath);
        $pathToFile = $storagePath . $filename;
        $outputFileName = storage_path('app/' . $pathToFile);
        $templateSheet = $spreadsheet->getSheetByName($this->worksheetList[Arr::get($this->config, $tab.'.sheet_index')]);
        // Get the highest row and column numbers referenced in the worksheet
        $highestData = $this->getHighestDataInfoBySheet($templateSheet);
        $dataCount = count($data);
        //row with headers columns
        $headersRow = Arr::get($this->config, $tab.".headers_row");
        $row = $headersRow + 1;
        $highestRow = $highestData["row"];

        //first need remove exists rows with products data
        /** default function removeRow($startRow, $rows) has issue (set empty all sheet if $rows >= last row with data)
         * $templateSheet->removeRow($row, $highestRow - $row); */
        /** this is bug in phpspreadsheet v1.11.0 */
//        while ($highestRow > $row) {
//            $templateSheet->removeRow($highestRow);
//            $highestRow--;
//        }
        if ($highestRow > $headersRow) {
            $templateSheet->removeRow($row, ($highestRow - $headersRow - 1)); // -1 is workaround because bug TODO: waiting for updates libruary
        }

        foreach ($data as $key => $properties) {
            for ($col = 1; $col <= $highestData["column_index"]; $col++) {
                try {
                    $val = $properties[$templateSheet->getCellByColumnAndRow($col, $headersRow)->getValue()];
                    $templateSheet->getCellByColumnAndRow($col, $row + $key)
                        ->setValue($val);
                } catch (\Exception $exception) {
                    Log::error($exception->__toString());
                }
            }
        }

        //save file through Xslx writer
        $writer = new XlsxWriter($spreadsheet);
        $writer->save($outputFileName);

        //return path saved file
        return $pathToFile;
    }
}

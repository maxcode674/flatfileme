<?php


namespace App\Services;


use App\Models\UserFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UserFileService
{
    protected $excelService;
    protected $user;

    public function __construct(ExcelService $excelService)
    {
        $this->excelService = $excelService;
    }

    /**
     * @param $request
     */
    public function uploadExcelFile($request)
    {
        $this->user = $request->user();
        $filename = $request->file('excel_file')->getClientOriginalName();
        $extension = $request->file('excel_file')->extension();
        $path = 'user_files/' . $this->user->id . '/files';

        try {
            $link = Storage::put($path, $request->file('excel_file'));
            $userFile = UserFile::create([
                'user_id' => $this->user->id,
                'filename' => $filename,
                'link' => $link,
                'type' => (int)$request->type
            ]);
            $this->excelService->prepareExcelData($userFile);

        } catch (\Exception $exception) {
            Log::alert($exception->getFile() . "\n" . $exception->getMessage());
        }
    }

    /**
     * @param UserFile $userFile
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function downloadExcelFile(UserFile $userFile)
    {
        //first of all remove all user files from local storage (previous downloading)
        $destroyPreviousPath = 'public/user_files/' . auth()->id();
        if (Storage::disk('local')->exists($destroyPreviousPath)) {
            Storage::disk('local')->deleteDirectory($destroyPreviousPath); //remove directory
        }
        //prepare file from storage and put it to local storage/app/public folder
        $localPath = 'public/' . $userFile->link;
        Storage::disk('local')->put($localPath, Storage::get($userFile->link));
        //get absolute path for copied file
        $absolutePath = storage_path('app/' . $localPath);
        //insert new data to 'Template' tab and return path of file.
        return $this->excelService->writeExcelData($userFile->excelData, $absolutePath, $userFile->filename, $userFile->type);
    }
}

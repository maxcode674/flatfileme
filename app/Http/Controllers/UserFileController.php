<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadFile\ExcelUploadRequest;
use App\Models\UserFile;
use App\Services\UserFileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UserFileController extends Controller
{
    /**
     * @var
     */
    protected $user;

    /**
     * @var UserFileService
     */
    protected $userFileService;

    /**
     * UserFileController constructor.
     * @param UserFileService $userFileService
     */
    public function __construct(UserFileService $userFileService)
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user(); // returns user
            return $next($request);
        });

        $this->userFileService = $userFileService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userFiles(Request $request)
    {
        if ($request->ajax()) {
            $userFilesQueryBuilder = $this->user->userFiles()
                ->join('excel_data', 'user_files.id', '=', 'excel_data.user_file_id')
                ->select('user_files.*', 'excel_data.user_file_id', 'excel_data.updated_at as modify_at');
            if ($request->has('sort_field')) {
                $sortDirection = $request->has('sort_direction') ? $request->sort_direction : 'desc';
                $userFilesQueryBuilder->orderBy($request->sort_field, $sortDirection);
            }
            return response()->json($userFilesQueryBuilder->simplePaginate());
        }

        return response()->json("Resource not found", 404);
    }

    /**
     * @param ExcelUploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadExcelFile(ExcelUploadRequest $request)
    {
        if ($request->hasFile('excel_file')) {
            $this->userFileService->uploadExcelFile($request);
        }

        return response()->json("File uploaded successfully");
    }

    /**
     * @param UserFile $userFile
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function downloadExcelFile(Request $request, UserFile $userFile)
    {
        //check is current user is owner
        if (!$request->ajax() || !$this->checkAccess($userFile)) {
            return response()->json('Forbidden', 403);
        }
        $pathToFile = $this->userFileService->downloadExcelFile($userFile);

        return response()->download(storage_path('app/' . $pathToFile));
    }

    /**
     * @param UserFile $userFile
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(UserFile $userFile)
    {
        //check is current user is owner
        if (!$this->checkAccess($userFile)) {
            return response()->json('Forbidden', 403);
        }

        try {
            Storage::delete($userFile->link);
            $userFile->delete();
        } catch (\Exception $exception) {
            Log::error($exception->__toString());
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json("Successfully deleted!", 200);
    }

    /**
     * @param UserFile $userFile
     * @return bool
     */
    public function checkAccess(UserFile $userFile)
    {
        return $userFile->user_id === $this->user->id || $this->user->isAdmin();
    }

    /**
     * @param Request $request
     * @param UserFile $userFile
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function getExcelDataFromFile(Request $request, UserFile $userFile)
    {
        //check is current user is owner
        if (!$request->ajax() || !$this->checkAccess($userFile)) {
            return response()->json('Forbidden', 403);
        }

        return response()->json(['excel_data' => $userFile->excelData, 'file_type' => $userFile->type]);
    }
}

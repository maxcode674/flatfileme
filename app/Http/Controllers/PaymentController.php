<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Payment;
use Stripe\PaymentIntent as StripePaymentIntent;

class PaymentController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserIntent(Request $request)
    {
        return response()->json(['intent' => $request->user()->createSetupIntent()]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function isSubscribed(Request $request)
    {
        $user = $request->user();
        if (!$request->user()->subscribed('default') && !$request->user()->isAdmin()) {
            if ($user->subscription('default') && $user->subscription('default')->hasIncompletePayment()) {
                $latestPayment = $this->getLatestPayment($request, false);
                if ($latestPayment['payment_method']) {
                    return response()
                        ->json(['message' => 'Please confirm your payment',
                            'link' => $latestPayment,
                            'status' => '']);
                }
            }
            return response()->json(['message' => 'You not subscribed yet, please subscribe first for use this site!',
                'link' => '', 'status' => '']);
        }

        if ($user->subscribed('default') && $user->subscription('default')->onGracePeriod()) {
            return response()->json([
                'message' => 'You are on a grace period till ' . $user->subscription('default')->ends_at->diffForHumans(),
                'link' => '',
                'status' => 'GRACE_PERIOD'
            ]);
        }

        return response()->json(['message' => '', 'link' => '', 'status' => '']);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function createSubscription(Request $request)
    {
        $user = $request->user();
        $planId = $user->isAdmin()
            ? config('payment_data.admin.monthly_plan.id')
            : config('payment_data.' . config('app.env') . '.monthly_plan.id');

        $options = [
            'email' => $user->email,
            'name' => $user->first_name . ' ' . $user->last_name
        ];

        try {
            $stripeCustomer = $user->createOrGetStripeCustomer($options);
            $result = $user->newSubscription('default', $planId)
                ->create($request->payment_method);
        } catch (\Exception $exception) {
            if ($user->subscription('default')->hasIncompletePayment()) {
                //this is second way of confirm pay (without redirect)
                return $this->getLatestPayment($request);
                // this is default Cashier way for confirm payment with 3DSecure, but it create card again in stripe customer cards (cashier issue v11.2.4)
                // return response()->json( ['redirect' => '/stripe/payment/'.$user->subscription('default')->latestPayment()->id], 301);
            }

            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPaymentMethod(Request $request)
    {
        $user = $request->user();
        $options = [
            'email' => $user->email,
            'name' => $user->first_name . ' ' . $user->last_name
        ];
        try {
            $stripeCustomer = $user->createOrGetStripeCustomer($options);
            $result = $user->addPaymentMethod($request->payment_method);
        } catch (\Exception $exception) {

            return response()->json(['errorsBag' => $exception], 500);
        }
        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeDefaultPaymentMethod(Request $request)
    {
        $user = $request->user();
        try {
            $user->updateDefaultPaymentMethod($request->payment_method);
            $defaultPaymentMethod = $user->defaultPaymentMethod()->id;
        } catch (\Exception $exception) {
            return response()->json(['errorsBag' => $exception], 500);
        }

        return response()->json(['defaultPaymentMethod' => $defaultPaymentMethod]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePaymentMethod(Request $request)
    {
        $user = $request->user();
        $paymentMethod = $user->findPaymentMethod($request->payment_method);
        if ($paymentMethod) {
            $isDefaultDelete = $request->payment_method === $user->defaultPaymentMethod()->id;

            $paymentMethod->delete();
            $user->updateDefaultPaymentMethodFromStripe();
            $paymentMethods = [];

            foreach ($user->paymentMethods() as $paymentMethod) {
                $paymentMethods[] = $paymentMethod->asStripePaymentMethod();
            }
            if (count($paymentMethods) && $isDefaultDelete) {
                $user->updateDefaultPaymentMethod($paymentMethods[0]->id);
            }
            return response()->json([
                'defaultPaymentMethod' => $user->defaultPaymentMethod()->id ?? null,
                'paymentMethods' => $paymentMethods,
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPaymentMethods(Request $request)
    {
        $user = $request->user();
        $defaultPaymentMethod = $user->defaultPaymentMethod()->id ?? null;
        $paymentMethods = [];
        foreach ($user->paymentMethods() as $paymentMethod) {
            $paymentMethods[] = $paymentMethod->asStripePaymentMethod();
        }
        return response()->json([
            'defaultPaymentMethod' => $defaultPaymentMethod ?? null,
            'paymentMethods' => $paymentMethods,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelSubscription(Request $request)
    {
        $user = $request->user();
        $user->subscription('default')->cancel();
        $expire = $user->subscription('default')->ends_at->diffForHumans();

        return response()->json('But you still can use site ' . $expire);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resumeSubscription(Request $request)
    {
        $user = $request->user();
        $user->subscription('default')->resume();

        return response()->json('Your subscription was resumed, Thank you!');
    }

    /**
     * @param Request $request
     * @param bool $json
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function getLatestPayment(Request $request, $json = true)
    {
        $user = $request->user();
        $confirmPayment = new Payment(
            StripePaymentIntent::retrieve(
                $user->subscription('default')->latestPayment()->id,
                Cashier::stripeOptions())
        );
        $client_secret = $confirmPayment->clientSecret();
        if ($json) {
            return response()->json([
                'need_confirm' => true,
                'client_secret' => $client_secret,
                'payment_method' => $confirmPayment->payment_method
            ], 302);
        } else {
            return [
                'need_confirm' => true,
                'client_secret' => $client_secret,
                'payment_method' => $confirmPayment->payment_method
            ];
        }

    }

}

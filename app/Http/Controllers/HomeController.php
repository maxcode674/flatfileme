<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $user = auth()->user();
        $userSubscribedPlan = $user->subscribed('default')
            ? $user->subscriptions->first()->stripe_plan
            : '';
        $plans = config('payment_data.' . config('app.env'));
        $paymentMethods = [];
        $defaultPaymentMethod = $user->defaultPaymentMethod()->id ?? null;
        foreach ($user->paymentMethods() as $paymentMethod) {
            $paymentMethods[] = $paymentMethod->asStripePaymentMethod();
        }

        return view('dashboard', [
            'plans' => $plans,
            'userSubscribedPlan' => $userSubscribedPlan,
            'paymentMethods' => $paymentMethods,
            'defaultPaymentMethod' => $defaultPaymentMethod
            ]);
    }

    /**
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $request->user();
        if (!(Hash::check($request->get('current_password'), $user->password))) {
            // The passwords matches
            return response()->json(["errors" => [
                "current_password" => ["Your current password does not matches with the password you provided. Please try again."]
            ]
            ], 422);
        }
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return response()->json(['message' => 'password was changed']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\ExcelData;
use Illuminate\Http\Request;
use App\Services\ExcelService;
use Illuminate\Support\Facades\Storage;

class ExcelDataController extends Controller
{
    /**
     * @var ExcelService
     */
    public $excelService;

    /**
     * ExcelDataController constructor.
     * @param ExcelService $excelService
     */
    public function __construct(ExcelService $excelService)
    {
        $this->excelService = $excelService;
    }

    /**
     * @param Request $request
     * @param ExcelData $excelData
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, ExcelData $excelData)
    {
        //Check if author of this file is current user
        if ($excelData->userFile->user->id === auth()->id()) {
            $excelData->update([
                "template" => $request->template
            ]);

            return response()->json("Updated successfully");
        }

        return response()->json("Forbidden", 403);
    }

    /**
     * @param Request $request
     * @param ExcelData $excelData
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImage(Request $request, ExcelData $excelData)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $storagePrefix = config('filesystems.default') == "local" ? 'public/' : '';
            $filePath = $storagePrefix . 'images/' . auth()->id() . '/' . $excelData->id;

            $link = Storage::putFileAs($filePath, $request->file('image'), $filename);
            $url = Storage::url($link);
            $prefixUrl = config('filesystems.default') == "local" ? env('APP_URL') : '';
            $fullUrl = $prefixUrl . $url;

            return response()->json(['link' => $fullUrl]);
        } else {
            return response()->json("File not uploaded", 500);
        }
    }
}

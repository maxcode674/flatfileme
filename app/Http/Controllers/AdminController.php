<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserFile;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adminSection()
    {
        return view('admin');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function listOfUsers(Request $request)
    {
        if ($request->ajax()) {
            $listOfUsersQueryBuilder = User::whereNotIn('id', [$request->user()->id])->withCount('userFiles');
            if ($request->has('sort_field')) {
                $sortDirection = $request->has('sort_direction') ? $request->sort_direction : 'desc';
                $listOfUsersQueryBuilder->orderBy($request->sort_field, $sortDirection);
            }
            return $listOfUsersQueryBuilder->simplePaginate();
        }

        return response()->json("Resource not found", 404);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginAsUser(User $user)
    {
        Auth::login($user);

        return response()->json('Success');
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function dashboardInfo(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'total_users' => $this->dashboardUsersInfo(),
                'total_uploads' => $this->dashboardUploadsInfo()
            ]);
        }

        return response()->json("Resource not found", 404);
    }

    /**
     * @return array
     */
    public function dashboardUsersInfo()
    {
        $users = User::whereNotIn('id', [auth()->id()])->get();
        return [
            'field_name' => 'Total costumers',
            'count' => $users->count(),
            'first_created_at' => $this->dateFormat($users->sortBy('created_at')->first()),
            'last_created_at' => $this->dateFormat($users->sortByDesc('created_at')->first()),
        ];
    }

    /**
     * @return array
     */
    public function dashboardUploadsInfo()
    {
        $userFiles = UserFile::all();
        return [
            'field_name' => 'Total uploaded',
            'count' => $userFiles->count(),
            'first_created_at' => $this->dateFormat($userFiles->sortBy('created_at')->first()),
            'last_created_at' => $this->dateFormat($userFiles->sortByDesc('created_at')->first()),
        ];
    }

    /**
     * @param $model
     * @param string $date_field
     * @return string|null
     */
    public function dateFormat($model, $date_field = 'created_at')
    {
        return !is_null($model) ? Carbon::parse($model->{$date_field})->format("M d Y") : null;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function latestUploadedFiles(Request $request)
    {
        if ($request->ajax()) {
            $latest = UserFile::orderBy('created_at', 'desc')
                ->with('user:id,last_name,first_name,email')
                ->limit(5)
                ->get();

            return response()->json($latest);
        }

        return response('Resource not found', 404);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function getStatistics(Request $request)
    {
        if ($request->ajax()) {
            $days = UserFile::orderBy('created_at')
                ->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->format('Y-m-d');
                })
                ->map
                ->count();
            return response()->json($days);
        }

        return response('Resource not found', 404);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class CheckSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && !$request->user()->subscribed('default') && !$request->user()->isAdmin()) {
            // This user is not a paying customer...
            return $request->ajax() || $request->wantsJson()
                ? response('Subscription Required.', 402)
                : redirect('dashboard');
        }

        return $next($request);
    }
}

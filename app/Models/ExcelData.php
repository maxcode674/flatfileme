<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExcelData extends Model
{
    protected $fillable = [
        'user_file_id', 'template', 'data_definitions', 'valid_values', 'item_model'
    ];

    protected $casts = [
        'template' => 'array',
        'data_definitions' => 'array',
        'valid_values' => 'array',
        'item_model' => 'array',
        'created_at' => "datetime:Y-m-d H:i",
        'updated_at' => "datetime:Y-m-d H:i",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userFile()
    {
        return $this->belongsTo(UserFile::class);
    }
}

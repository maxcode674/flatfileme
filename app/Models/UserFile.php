<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFile extends Model
{
    public const FLAT_FILE = 1;
    public const LISTING_REPORT = 2;

    public $listsOfTypes = [
        1 => 'Flat file',
        2 => 'Listing report'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'filename', 'link', 'type'
    ];

    protected $casts = [
        'created_at' => "datetime:Y-m-d H:i",
        'updated_at' => "datetime:Y-m-d H:i",
    ];

    protected $appends = [
        'type_name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function excelData()
    {
        return $this->hasOne(ExcelData::class);
    }

    public function getTypeNameAttribute()
    {
        return $this->listsOfTypes[$this->type];
    }

}

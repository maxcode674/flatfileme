@extends('layouts.app')

@section('content')

@auth
    <user-dashboard-component
        :plans="{{json_encode($plans)}}"
        :user-subscribed-plan="{{json_encode($userSubscribedPlan)}}"
        :payment-methods="{{json_encode($paymentMethods)}}"
        :default-payment-method="{{json_encode($defaultPaymentMethod)}}"
    ></user-dashboard-component>
@endauth

@endsection

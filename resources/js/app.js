/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./general/spinner');

window.Vue = require('vue');
window.Swal = require('sweetalert2');
import { ValidationProvider } from 'vee-validate';
require('./validations');
import VTooltip from 'v-tooltip'
Vue.use(VTooltip);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('admin-component', require('./components/AdminComponent.vue').default);
Vue.component('excel-files', require('./components/ExcelFilesComponent.vue').default);
Vue.component('template-edit', require('./components/TemplateEditComponent.vue').default);
Vue.component('switcher-component', require('./components/SwitcherComponent.vue').default);
Vue.component('list-of-users', require('./components/ListOfUsersComponent.vue').default);
Vue.component('dashboard-component', require('./components/DashboardComponent.vue').default);
Vue.component('user-dashboard-component', require('./components/UserDashboardComponent.vue').default);
Vue.component('analysis-component', require('./components/AnalysisComponent.vue').default);
Vue.component('payment-component', require('./components/PaymentComponent.vue').default);
Vue.component('item-component', require('./components/ItemComponent.vue').default);
Vue.component('ValidationProvider', ValidationProvider);

// event bus
export const EventBus = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

window.spinner = (show = true) => {
    show ? $('.spinner').fadeIn(100) : $('.spinner').fadeOut(1000);
};

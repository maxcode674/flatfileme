import { extend } from 'vee-validate';
import { required, numeric, alpha_num, oneOf } from 'vee-validate/dist/rules';
import moment from "moment";

extend('positive', value => {
    if (value >= 0) {
        return true;
    }
    return 'The {_field_} field must be a positive number';
});

extend('required', {
    ...required,
    message: 'The {_field_} is required'
});

//example 'oneOf:1,2,3,4,5' or 'oneOf:apple,banana,cherry' -> can be empty
extend('oneOf', {
    ...oneOf,
    message: 'The {_field_} is wrong chose'
});

extend('numeric', {
    ...numeric,
    message: 'The {_field_} field may only contain numeric characters'
});

extend('alpha_num', value =>{
    let expr = new RegExp('^[a-zA-Z0-9 .,()-–_\']*$');
    if (expr.test(value)) {
        return true;
    }
    return 'The {_field_} field may only contain alpha-numeric characters';
});

// example: 'minmax:1,10' mean string length from 1 to 10 max characters
extend('minmax', {
    validate(value, { min, max }) {
        return value.length >= min && value.length <= max;
    },
    params: ['min', 'max'],
    message: 'The {_field_} field must have at least {min} characters and {max} characters at most'
});

extend('price', value => {
    let expr = new RegExp('^\\d{0,8}(\\.\\d{1,2})?$');
    if (expr.test(value)) {
        return true;
    }
    return 'The {_field_} field must be a price with two decimals';
});

extend('url', value => {
    let expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    let expr = new RegExp(expression);
    if (expr.test(value)) {
        return true;
    }
    return 'The {_field_} field must be valid url including leading \'http(s)://\' ';
});

extend('six_digits_or_nv', value =>{
    let expr = new RegExp('^([0-9]{1,6}$)|(NV)$');
    if (expr.test(value)) {
        return true;
    }
    return 'The {_field_} field must be 6 digit numeric value or NV';
});

// example: 'float:4' mean with max 4 digits after '.'
extend('float', {
    validate(value, { max }) {
        let expr = new RegExp('^\\d{0,8}(\\.\\d{1,'+ max +'})?$');
        return expr.test(value);
    },
    params: ['max'],
    message: 'The {_field_} field must be float with {max} digits for fraction'
});

extend('date', {
    validate(value, { max }) {
        let expr = new RegExp('^(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$');
        return expr.test(value);
    },
    params: ['max'],
    message: 'The {_field_} field must be a date in yyyy-mm-dd format'
});

//example 'date_format:YYYY-MM-DD'
extend('date_format', {
    validate(value, { format }) {
        return moment(value, format, true).isValid()
    },
    params: ['format'],
    message: 'The {_field_} field must be a date in {format} format'
});


<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExcelDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excel_data', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_file_id');
            $table->foreign('user_file_id')
                ->references('id')
                ->on('user_files')
                ->onDelete('cascade');
            $table->longText('template');
            $table->longText('data_definitions');
            $table->longText('valid_values');
            $table->longText('item_model');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excel_data');
    }
}

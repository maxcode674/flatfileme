## Setup:

1. Clone project:
    - `git clone https://maxcode674@bitbucket.org/maxcode674/flatfileme.git`

2. Set permissions:
    - `sudo chown -R www-data:www-data path/to/project`
    - `sudo chmod -R 775 path/to/project/storage`
    - `sudo chmod -R 775 path/to/project/bootstrap/cache`
    - Add self to group www-data: `sudo usermod -a -G www-data $USER`

3. Copy .env file and set environment variables:
    - `cp path/to/project/.env.example path/to/project/.env`

4. Composer install:
    - `composer install`

5. Generate key: 
    - `php artisan key:generate`
    
6. Setup new database and fill credentials in .env
    - `DB_CONNECTION=mysql`
    - `DB_HOST=127.0.0.1`
    - `DB_PORT=3306`
    - `DB_DATABASE=your_database_name`
    - `DB_USERNAME=your_mysql_user_name`
    - `DB_PASSWORD=your_mysql_user_password`

7. Npm install:
    - `npm install`

8. Compile assets:
    - `npm run dev`

9. Setup filesystem (AWS s3) .env
    - `FILESYSTEM_DRIVER=s3`
    - `AWS_ACCESS_KEY_ID=your_acces_key_id`
    - `AWS_SECRET_ACCESS_KEY=your_secret_access_key`
    - `AWS_DEFAULT_REGION=us-west-2`
    - `AWS_BUCKET=your_bucket_name`

####More info about configuration on official [Laravel site](https://laravel.com/docs/7.x#configuration).

####Issues phpspreadsheets (1.11.0):

if cell is blocked(protected) then it's background set black. It found when manage .xlsm file.
so temporary solution is fix vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Reader/Xlsx.php@1624

`if ($style->fill->patternFill->fgColor) {
                     $docStyle->getFill()->getStartColor()->setARGB(self::readColor($style->fill->patternFill->fgColor, true));
                 } else {
                     $docStyle->getFill()->getStartColor()->setARGB('FF000000'); //this is line 1624   
                     $docStyle->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_NONE); //this what need instead line 1624
                 }`


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

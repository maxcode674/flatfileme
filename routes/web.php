<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('check.subscription');

Auth::routes();

Route::group(["middleware" => "auth"], function ($route) {

    /** User dashboard */
    $route->get('dashboard', 'HomeController@dashboard')->name('dashboard');
    $route->post('is-subscribed', 'PaymentController@isSubscribed')->name('is-subscribed');
    $route->post('change-password', 'HomeController@changePassword')->name('change-password');

    /** Payment section */
    $route->post('get-user-intent', 'PaymentController@getUserIntent')->name('get-user-intent');
    $route->post('create-subscription', 'PaymentController@createSubscription')->name('create-subscription');
    $route->post('cancel-subscription', 'PaymentController@cancelSubscription')->name('cancel-subscription');
    $route->post('restore-subscription', 'PaymentController@resumeSubscription')->name('resume-subscription');
    $route->post('add-payment-method', 'PaymentController@addPaymentMethod')->name('add-payment-method');
    $route->post('change-default-payment-method', 'PaymentController@changeDefaultPaymentMethod')->name('change-default-payment-method');
    $route->post('delete-payment-method', 'PaymentController@deletePaymentMethod')->name('delete-payment-method');
    $route->post('get-payment-methods' ,'PaymentController@getPaymentMethods')->name('get-payment-method');

});

Route::group(["middleware" => ["auth", "check.subscription"]], function ($route) {

    /** UserFiles routes */
    $route->get('user-files', 'UserFileController@userFiles')->name('user-files');
    $route->post('upload-excel-file', 'UserFileController@uploadExcelFile')->name('upload-excel-file');
    $route->get('download-excel-file/{userFile}', 'UserFileController@downloadExcelFile')->name('download-excel-file');
    $route->delete('delete-excel-file/{userFile}', 'UserFileController@destroy')->name('delete-excel-file');
    $route->get('excel-data-from-file/{userFile}', 'UserFileController@getExcelDataFromFile')->name('excel-data-from-file');

    /** Excel working routes */
    $route->post('excel-data/{excelData}', 'ExcelDataController@update');
    $route->post('upload-image/{excelData}', 'ExcelDataController@uploadImage')->name('upload-image');
});

Route::group(["middleware" => ["auth", "can:admin-management"]], function ($route) {

    /** Admin section*/
    $route->get('admin-section', 'AdminController@adminSection')->name('admin-section');
    $route->get('list-of-users', 'AdminController@listOfUsers')->name('list-of-users');
    $route->post('login-as/{user}', 'AdminController@loginAsUser')->name('login-as-user');
    $route->get('dashboard-info', 'AdminController@dashboardInfo')->name('dashboard-info');
    $route->get('latest-files', 'AdminController@latestUploadedFiles')->name('latest-files');
    $route->get('statistics', 'AdminController@getStatistics')->name('statistics');

});
Route::get('/home', 'HomeController@index')->name('home');
